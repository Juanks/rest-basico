const { response, request } = require('express')

const usuarioGet = (req = request, res = response) => {
    const query = req.query;
    //desestructurar
    //http://localhost:8081/api/usuarios?anio=2&apikey=54564sdf&page=2&limit=3
    const { nombre = 'no se envio', apikey, anio, page = 1, limit } = req.query
    res.json({
        "msg": 'GET API - CONTROLADOR',
        query,
        nombre,
        apikey,
        anio,
        page,
        limit
    });
}

const usuariosPost = (req, res) => {
    const body = req.body;
    console.log("Datos del request:")
    console.log(body)
    //desestructurando
    const { nombre, edad } = req.body

    res.json({
        ok: true,
        msg: 'POST API - CONTROLADOR',
        body,
        nombre
    })
}

const usuariosPut = (req, res = response) => {
    //capturando como obj
    const id_1 = req.params;
    //desestructurando
    const { id } = req.params

    res.json({
        "ok": true,
        "msg": 'PUT API - CONTROLADORT',
        id_1,
        id
    })
}
const usuariosPatch = (req, res) => {
    res.json({ "ok": true, "msg": 'PATCH API - CONTROLADOR' })
}
const usuariosDelete = (req, res) => {
    res.json({ "ok": true, "msg": 'DELETE API - CONTROLADOR' })
}


module.exports = {
    usuarioGet,
    usuariosPost,
    usuariosPut,
    usuariosPatch,
    usuariosDelete
}